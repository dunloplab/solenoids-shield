To order new PCBs: 
Go to OSH Park (https://oshpark.com/)
Upload the .kicad_pcb file you want to get printed.
Default settings are typically fine.

You can load main\_code.ino into the arduino if you plan on using the shield with valves and also the solenoid pumps from the Lee co. If you will only use valves, all\_valves.ino will allow you to use all outputs as valves.

If you did not solder all the A0, A1, A2 pins on the board to GND, you will need to change the shield's adress at the beginning of the .ino files.


The serial commands, both for main\_code and all\_valves, are as follows:

VALvvvSs / sprintf("VAL%03dS%1d", valvenb, valvestate):  
Send command to switch valve number vvv (valvenb) to state s (valvestate, binary)

PWMvvvSs%cccP\[ppp\] / sprintf("PWM%03dS%1d\%%03dP%d", valvenb, pwmstate, pwmduty, pwmperiod):  
Set arduino to turn valve vvv (valvenb) on/off periodically, if state s (pwmstate, binary) is equal to '1'. Period is given by ppppp (pwmperiod, no pre-defined length) in milliseconds. The duty cycle of the PWM is given by ccc, in percent (pwmduty).

PUMuuuSsP\[ppp\] / sprintf("PUM%03dS%1dP%d", pumpnb, pumpstate, pumpperiod): _only for main\_code_  
Set arduino to "crank" pump uuu (pumpnb) on periodically, if state s (pumpstate, binary) is equal to '1'. Period is given by ppppp (pumpperiod, no pre-defined length) in milliseconds.
