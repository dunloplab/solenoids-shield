EESchema Schematic File Version 4
LIBS:solenoids_shield-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_Interface-I-O-Expanders:MCP23017-E_SP M001
U 1 1 5C47410F
P 1650 1700
F 0 "M001" H 1750 2700 60  0000 C CNN
F 1 "MCP23017-E_SP" H 1750 2594 60  0000 C CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 1850 1900 60  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en023709" H 1850 2000 60  0001 L CNN
F 4 "MCP23017-E/SP-ND" H 1850 2100 60  0001 L CNN "Digi-Key_PN"
F 5 "MCP23017-E/SP" H 1850 2200 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 1850 2300 60  0001 L CNN "Category"
F 7 "Interface - I/O Expanders" H 1850 2400 60  0001 L CNN "Family"
F 8 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en023709" H 1850 2500 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/microchip-technology/MCP23017-E-SP/MCP23017-E-SP-ND/894272" H 1850 2600 60  0001 L CNN "DK_Detail_Page"
F 10 "IC I/O EXPANDER I2C 16B 28SDIP" H 1850 2700 60  0001 L CNN "Description"
F 11 "Microchip Technology" H 1850 2800 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1850 2900 60  0001 L CNN "Status"
	1    1650 1700
	1    0    0    -1  
$EndComp
$Comp
L Transistor_Array:ULN2003A U01
U 1 1 5C4743BD
P 3400 1300
F 0 "U01" H 3400 1967 50  0000 C CNN
F 1 "ULN2003A" H 3400 1876 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 3450 750 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 3500 1100 50  0001 C CNN
	1    3400 1300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5C479167
P 2250 900
F 0 "#PWR0101" H 2250 750 50  0001 C CNN
F 1 "+5V" H 2265 1073 50  0000 C CNN
F 2 "" H 2250 900 50  0001 C CNN
F 3 "" H 2250 900 50  0001 C CNN
	1    2250 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 1300 2250 1300
Wire Wire Line
	2250 1300 2250 900 
Wire Wire Line
	1750 900  2250 900 
Connection ~ 2250 900 
Text GLabel 1050 2900 0    50   Input ~ 0
address0
Text GLabel 1050 3000 0    50   Input ~ 0
address1
Text GLabel 1050 3100 0    50   Input ~ 0
address2
Text GLabel 1050 2800 0    50   Input ~ 0
scl
Text GLabel 1050 2700 0    50   Input ~ 0
sda
$Comp
L power:GND #PWR0102
U 1 1 5C479410
P 1750 3400
F 0 "#PWR0102" H 1750 3150 50  0001 C CNN
F 1 "GND" H 1755 3227 50  0000 C CNN
F 2 "" H 1750 3400 50  0001 C CNN
F 3 "" H 1750 3400 50  0001 C CNN
	1    1750 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 3300 1750 3400
Wire Wire Line
	1050 2700 1350 2700
Wire Wire Line
	1050 2800 1350 2800
Wire Wire Line
	1050 2900 1350 2900
Wire Wire Line
	1050 3000 1350 3000
Wire Wire Line
	1050 3100 1350 3100
Text GLabel 1200 1100 0    50   Input ~ 0
pin00
Text GLabel 1200 1200 0    50   Input ~ 0
pin01
Text GLabel 1200 1300 0    50   Input ~ 0
pin02
Text GLabel 1200 1400 0    50   Input ~ 0
pin03
Text GLabel 1200 1500 0    50   Input ~ 0
pin04
Text GLabel 1200 1600 0    50   Input ~ 0
pin05
Text GLabel 1200 1700 0    50   Input ~ 0
pin06
Text GLabel 1200 1800 0    50   Input ~ 0
pin07
Text GLabel 1200 1900 0    50   Input ~ 0
pin08
Text GLabel 1200 2000 0    50   Input ~ 0
pin09
Text GLabel 1200 2100 0    50   Input ~ 0
pin10
Text GLabel 1200 2200 0    50   Input ~ 0
pin11
Text GLabel 1200 2300 0    50   Input ~ 0
pin12
Text GLabel 1200 2400 0    50   Input ~ 0
pin13
Text GLabel 1200 2500 0    50   Input ~ 0
pin14
Text GLabel 1200 2600 0    50   Input ~ 0
pin15
Wire Wire Line
	1200 1100 1350 1100
Wire Wire Line
	1200 1200 1350 1200
Wire Wire Line
	1200 1300 1350 1300
Wire Wire Line
	1200 1400 1350 1400
Wire Wire Line
	1200 1500 1350 1500
Wire Wire Line
	1200 1600 1350 1600
Wire Wire Line
	1200 1700 1350 1700
Wire Wire Line
	1200 1800 1350 1800
Wire Wire Line
	1200 1900 1350 1900
Wire Wire Line
	1200 2000 1350 2000
Wire Wire Line
	1200 2100 1350 2100
Wire Wire Line
	1200 2200 1350 2200
Wire Wire Line
	1200 2300 1350 2300
Wire Wire Line
	1200 2400 1350 2400
Wire Wire Line
	1200 2500 1350 2500
Wire Wire Line
	1200 2600 1350 2600
$Comp
L power:+12V #PWR0103
U 1 1 5C47B747
P 3900 800
F 0 "#PWR0103" H 3900 650 50  0001 C CNN
F 1 "+12V" H 3915 973 50  0000 C CNN
F 2 "" H 3900 800 50  0001 C CNN
F 3 "" H 3900 800 50  0001 C CNN
	1    3900 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 900  3900 900 
Wire Wire Line
	3900 900  3900 800 
$Comp
L power:GND #PWR0104
U 1 1 5C47BBD7
P 3400 2000
F 0 "#PWR0104" H 3400 1750 50  0001 C CNN
F 1 "GND" H 3405 1827 50  0000 C CNN
F 2 "" H 3400 2000 50  0001 C CNN
F 3 "" H 3400 2000 50  0001 C CNN
	1    3400 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1900 3400 2000
Text GLabel 2750 1100 0    50   Input ~ 0
pin00
Text GLabel 2750 1300 0    50   Input ~ 0
pin01
Text GLabel 2750 1500 0    50   Input ~ 0
pin02
Text GLabel 2750 1700 0    50   Input ~ 0
pin03
Wire Wire Line
	2750 1100 3000 1100
Wire Wire Line
	2750 1300 3000 1300
Wire Wire Line
	2750 1500 3000 1500
Wire Wire Line
	2750 1700 3000 1700
$Comp
L Transistor_Array:ULN2003A U03
U 1 1 5C47FD3F
P 3400 3050
F 0 "U03" H 3400 3717 50  0000 C CNN
F 1 "ULN2003A" H 3400 3626 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 3450 2500 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 3500 2850 50  0001 C CNN
	1    3400 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5C47FD4D
P 3400 3750
F 0 "#PWR0105" H 3400 3500 50  0001 C CNN
F 1 "GND" H 3405 3577 50  0000 C CNN
F 2 "" H 3400 3750 50  0001 C CNN
F 3 "" H 3400 3750 50  0001 C CNN
	1    3400 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 3650 3400 3750
Text GLabel 2750 2850 0    50   Input ~ 0
pin08
Text GLabel 2750 3050 0    50   Input ~ 0
pin09
Text GLabel 2750 3250 0    50   Input ~ 0
pin10
Text GLabel 2750 3450 0    50   Input ~ 0
pin11
Wire Wire Line
	2750 2850 3000 2850
Wire Wire Line
	2750 3050 3000 3050
Wire Wire Line
	2750 3250 3000 3250
Wire Wire Line
	2750 3450 3000 3450
$Comp
L Transistor_Array:ULN2003A U02
U 1 1 5C48091F
P 5650 1300
F 0 "U02" H 5650 1967 50  0000 C CNN
F 1 "ULN2003A" H 5650 1876 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 5700 750 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 5750 1100 50  0001 C CNN
	1    5650 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5C48092D
P 5650 2000
F 0 "#PWR0106" H 5650 1750 50  0001 C CNN
F 1 "GND" H 5655 1827 50  0000 C CNN
F 2 "" H 5650 2000 50  0001 C CNN
F 3 "" H 5650 2000 50  0001 C CNN
	1    5650 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1900 5650 2000
Text GLabel 5000 1100 0    50   Input ~ 0
pin04
Text GLabel 5000 1300 0    50   Input ~ 0
pin05
Text GLabel 5000 1500 0    50   Input ~ 0
pin06
Text GLabel 5000 1700 0    50   Input ~ 0
pin07
Wire Wire Line
	5000 1100 5250 1100
Wire Wire Line
	5000 1300 5250 1300
Wire Wire Line
	5000 1500 5250 1500
Wire Wire Line
	5000 1700 5250 1700
$Comp
L Transistor_Array:ULN2003A U04
U 1 1 5C4819B1
P 5650 3050
F 0 "U04" H 5650 3717 50  0000 C CNN
F 1 "ULN2003A" H 5650 3626 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 5700 2500 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 5750 2850 50  0001 C CNN
	1    5650 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5C4819BF
P 5650 3750
F 0 "#PWR0107" H 5650 3500 50  0001 C CNN
F 1 "GND" H 5655 3577 50  0000 C CNN
F 2 "" H 5650 3750 50  0001 C CNN
F 3 "" H 5650 3750 50  0001 C CNN
	1    5650 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3650 5650 3750
Text GLabel 5000 2850 0    50   Input ~ 0
pin12
Text GLabel 5000 3050 0    50   Input ~ 0
pin13
Text GLabel 5000 3250 0    50   Input ~ 0
pin14
Text GLabel 5000 3450 0    50   Input ~ 0
pin15
Wire Wire Line
	5000 2850 5250 2850
Wire Wire Line
	5000 3050 5250 3050
Wire Wire Line
	5000 3250 5250 3250
Wire Wire Line
	5000 3450 5250 3450
$Comp
L Connector:Screw_Terminal_01x08 J01
U 1 1 5C487973
P 4250 1300
F 0 "J01" H 4330 1292 50  0000 L CNN
F 1 "Terminal" H 4330 1201 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-8-2.54_1x08_P2.54mm_Horizontal" H 4250 1300 50  0001 C CNN
F 3 "~" H 4250 1300 50  0001 C CNN
	1    4250 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 900  3900 1000
Wire Wire Line
	3900 1000 4050 1000
Connection ~ 3900 900 
Wire Wire Line
	3900 1000 3900 1200
Wire Wire Line
	3900 1200 4050 1200
Connection ~ 3900 1000
Wire Wire Line
	3900 1200 3900 1400
Wire Wire Line
	3900 1400 4050 1400
Connection ~ 3900 1200
Wire Wire Line
	3900 1400 3900 1600
Wire Wire Line
	3900 1600 4050 1600
Connection ~ 3900 1400
Wire Wire Line
	3800 1300 4050 1300
Wire Wire Line
	3800 1500 4050 1500
Wire Wire Line
	3800 1100 4050 1100
Wire Wire Line
	3800 1700 4050 1700
$Comp
L power:+12V #PWR0108
U 1 1 5C496F58
P 6150 800
F 0 "#PWR0108" H 6150 650 50  0001 C CNN
F 1 "+12V" H 6165 973 50  0000 C CNN
F 2 "" H 6150 800 50  0001 C CNN
F 3 "" H 6150 800 50  0001 C CNN
	1    6150 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 900  6150 900 
Wire Wire Line
	6150 900  6150 800 
$Comp
L Connector:Screw_Terminal_01x08 J02
U 1 1 5C496F60
P 6500 1300
F 0 "J02" H 6580 1292 50  0000 L CNN
F 1 "Terminal" H 6580 1201 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-8-2.54_1x08_P2.54mm_Horizontal" H 6500 1300 50  0001 C CNN
F 3 "~" H 6500 1300 50  0001 C CNN
	1    6500 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 900  6150 1000
Wire Wire Line
	6150 1000 6300 1000
Connection ~ 6150 900 
Wire Wire Line
	6150 1000 6150 1200
Wire Wire Line
	6150 1200 6300 1200
Connection ~ 6150 1000
Wire Wire Line
	6150 1200 6150 1400
Wire Wire Line
	6150 1400 6300 1400
Connection ~ 6150 1200
Wire Wire Line
	6150 1400 6150 1600
Wire Wire Line
	6150 1600 6300 1600
Connection ~ 6150 1400
Wire Wire Line
	6050 1300 6300 1300
Wire Wire Line
	6050 1500 6300 1500
Wire Wire Line
	6050 1100 6300 1100
Wire Wire Line
	6050 1700 6300 1700
$Comp
L power:+12V #PWR0109
U 1 1 5C49AA84
P 3900 2550
F 0 "#PWR0109" H 3900 2400 50  0001 C CNN
F 1 "+12V" H 3915 2723 50  0000 C CNN
F 2 "" H 3900 2550 50  0001 C CNN
F 3 "" H 3900 2550 50  0001 C CNN
	1    3900 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2650 3900 2650
Wire Wire Line
	3900 2650 3900 2550
$Comp
L Connector:Screw_Terminal_01x08 J03
U 1 1 5C49AA8C
P 4250 3050
F 0 "J03" H 4330 3042 50  0000 L CNN
F 1 "Terminal" H 4330 2951 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-8-2.54_1x08_P2.54mm_Horizontal" H 4250 3050 50  0001 C CNN
F 3 "~" H 4250 3050 50  0001 C CNN
	1    4250 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2650 3900 2750
Wire Wire Line
	3900 2750 4050 2750
Connection ~ 3900 2650
Wire Wire Line
	3900 2750 3900 2950
Wire Wire Line
	3900 2950 4050 2950
Connection ~ 3900 2750
Wire Wire Line
	3900 2950 3900 3150
Wire Wire Line
	3900 3150 4050 3150
Connection ~ 3900 2950
Wire Wire Line
	3900 3150 3900 3350
Wire Wire Line
	3900 3350 4050 3350
Connection ~ 3900 3150
Wire Wire Line
	3800 3050 4050 3050
Wire Wire Line
	3800 3250 4050 3250
Wire Wire Line
	3800 2850 4050 2850
Wire Wire Line
	3800 3450 4050 3450
$Comp
L power:+12V #PWR0110
U 1 1 5C49F211
P 6150 2550
F 0 "#PWR0110" H 6150 2400 50  0001 C CNN
F 1 "+12V" H 6165 2723 50  0000 C CNN
F 2 "" H 6150 2550 50  0001 C CNN
F 3 "" H 6150 2550 50  0001 C CNN
	1    6150 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2650 6150 2650
Wire Wire Line
	6150 2650 6150 2550
$Comp
L Connector:Screw_Terminal_01x08 J04
U 1 1 5C49F219
P 6500 3050
F 0 "J04" H 6580 3042 50  0000 L CNN
F 1 "Terminal" H 6580 2951 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-8-2.54_1x08_P2.54mm_Horizontal" H 6500 3050 50  0001 C CNN
F 3 "~" H 6500 3050 50  0001 C CNN
	1    6500 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2650 6150 2750
Wire Wire Line
	6150 2750 6300 2750
Connection ~ 6150 2650
Wire Wire Line
	6150 2750 6150 2950
Wire Wire Line
	6150 2950 6300 2950
Connection ~ 6150 2750
Wire Wire Line
	6150 2950 6150 3150
Wire Wire Line
	6150 3150 6300 3150
Connection ~ 6150 2950
Wire Wire Line
	6150 3150 6150 3350
Wire Wire Line
	6150 3350 6300 3350
Connection ~ 6150 3150
Wire Wire Line
	6050 3050 6300 3050
Wire Wire Line
	6050 3250 6300 3250
Wire Wire Line
	6050 2850 6300 2850
Wire Wire Line
	6050 3450 6300 3450
$Comp
L Connector_Generic:Conn_01x08 J08_01
U 1 1 5C4A48A1
P 9900 3100
F 0 "J08_01" H 9980 3092 50  0000 L CNN
F 1 "Conn_01x08" H 9980 3001 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 9900 3100 50  0001 C CNN
F 3 "~" H 9900 3100 50  0001 C CNN
	1    9900 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J08_02
U 1 1 5C4A4A19
P 7950 2050
F 0 "J08_02" H 7870 2567 50  0000 C CNN
F 1 "Conn_01x08" H 7870 2476 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 7950 2050 50  0001 C CNN
F 3 "~" H 7950 2050 50  0001 C CNN
	1    7950 2050
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J06_01
U 1 1 5C4A4C23
P 7950 3100
F 0 "J06_01" H 7870 3517 50  0000 C CNN
F 1 "Conn_01x06" H 7870 3426 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 7950 3100 50  0001 C CNN
F 3 "~" H 7950 3100 50  0001 C CNN
	1    7950 3100
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J10_01
U 1 1 5C4A4DC4
P 9900 2050
F 0 "J10_01" H 9980 2042 50  0000 L CNN
F 1 "Conn_01x10" H 9980 1951 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x10_P2.54mm_Vertical" H 9900 2050 50  0001 C CNN
F 3 "~" H 9900 2050 50  0001 C CNN
	1    9900 2050
	1    0    0    -1  
$EndComp
Text GLabel 9550 1650 0    50   Input ~ 0
scl
Text GLabel 9550 1750 0    50   Input ~ 0
sda
Wire Wire Line
	9550 1650 9700 1650
Wire Wire Line
	9550 1750 9700 1750
$Comp
L power:GND #PWR0111
U 1 1 5C4AA592
P 8900 3650
F 0 "#PWR0111" H 8900 3400 50  0001 C CNN
F 1 "GND" H 8905 3477 50  0000 C CNN
F 2 "" H 8900 3650 50  0001 C CNN
F 3 "" H 8900 3650 50  0001 C CNN
	1    8900 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0112
U 1 1 5C4AA9E8
P 9000 1600
F 0 "#PWR0112" H 9000 1450 50  0001 C CNN
F 1 "+12V" H 9015 1773 50  0000 C CNN
F 2 "" H 9000 1600 50  0001 C CNN
F 3 "" H 9000 1600 50  0001 C CNN
	1    9000 1600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0113
U 1 1 5C4AAA38
P 8650 1600
F 0 "#PWR0113" H 8650 1450 50  0001 C CNN
F 1 "+5V" H 8665 1773 50  0000 C CNN
F 2 "" H 8650 1600 50  0001 C CNN
F 3 "" H 8650 1600 50  0001 C CNN
	1    8650 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 2450 9000 2450
Wire Wire Line
	9000 2450 9000 1600
Wire Wire Line
	8150 2250 8900 2250
Wire Wire Line
	8900 2250 8900 2350
Wire Wire Line
	8150 2350 8900 2350
Connection ~ 8900 2350
Wire Wire Line
	8900 2350 8900 3650
Wire Wire Line
	8150 2150 8650 2150
Wire Wire Line
	8650 2150 8650 1600
Wire Notes Line
	7650 4050 7650 1150
Wire Notes Line
	7650 1150 10100 1150
Wire Notes Line
	10100 1150 10100 4050
Wire Notes Line
	10100 4050 7650 4050
Text Notes 8300 1100 0    50   ~ 0
Arduino stackable headers
$Comp
L jb_solderselector_01x03:jb_solderselector_01x03 S01
U 1 1 5C4CF7A6
P 2750 5500
F 0 "S01" H 2878 5546 50  0000 L CNN
F 1 "jb_solderselector_01x03" H 2878 5455 50  0000 L CNN
F 2 "solenoids_shield:solderselector" H 2950 4750 50  0001 C CNN
F 3 "" H 2950 4750 50  0001 C CNN
	1    2750 5500
	1    0    0    -1  
$EndComp
Text GLabel 2350 5400 0    50   Input ~ 0
address0
Text GLabel 2350 5500 0    50   Input ~ 0
address1
Text GLabel 2350 5600 0    50   Input ~ 0
address2
$Comp
L power:GND #PWR0114
U 1 1 5C4CFAFC
P 2750 6050
F 0 "#PWR0114" H 2750 5800 50  0001 C CNN
F 1 "GND" H 2755 5877 50  0000 C CNN
F 2 "" H 2750 6050 50  0001 C CNN
F 3 "" H 2750 6050 50  0001 C CNN
	1    2750 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 5950 2750 6050
$Comp
L power:+5V #PWR0115
U 1 1 5C4D2DCF
P 2750 4950
F 0 "#PWR0115" H 2750 4800 50  0001 C CNN
F 1 "+5V" H 2765 5123 50  0000 C CNN
F 2 "" H 2750 4950 50  0001 C CNN
F 3 "" H 2750 4950 50  0001 C CNN
	1    2750 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 5050 2750 4950
Wire Wire Line
	2350 5400 2600 5400
Wire Wire Line
	2350 5500 2600 5500
Wire Wire Line
	2350 5600 2600 5600
$EndSCHEMATC
