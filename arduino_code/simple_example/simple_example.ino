/*
  simple_example

  This code connects and initializes one shield (with address set to 000, otherwise change the SHIELD_ADRESS value to 1, 2, 3, etc...)
  It sets the solenoid outputs states to LOW, and once setup is done, it cycles through the outputs and sets them to HIGH sequentially
*/

// Make sure you installed the Adafruit MCP23017 library 
// (Sketch > Include Library > Manage Libraries... > *search for MCP23017, select Adafruit one* > Install)
#include <Wire.h>
#include <Adafruit_MCP23017.h>

#define SHIELD_ADDRESS 0 // SHIELD/MCP23017 address (change to 1-8 if soldered differently)
#define BAUD_RATE 9600
#define PERIOD 200 // output cycling period, in ms (>=200 if using Lee Co. LPM pumps)


Adafruit_MCP23017 mcp;

// State variables
word mcpstate = 0;


void setup()
{
  // Intialize serial connection and LED pin
  Serial.println("Starting up...");
  Serial.begin(BAUD_RATE);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN,HIGH);

  // Connect to MCP
  Serial.println("Initializing i2c for mcp23017 comm...");
  mcp.begin(SHIELD_ADDRESS);

  
  // Set MCP pins as outputs
  Serial.println("Setting up MCP pins to outputs...");
  for (int i = 0; i<16;i++)
  {
    delay(50); // delay necessary between pinMode commands
    digitalWrite(LED_BUILTIN,LOW);
    mcp.pinMode(i, OUTPUT);
    delay(50);
    digitalWrite(LED_BUILTIN,HIGH);
  }

  // Write init state to MCP
  Serial.println("Setting up MCP pins init state...");
  mcp.writeGPIOAB(mcpstate);

  
  Serial.println("Setup done!");
}

void loop() {

  delay(PERIOD/2);
  digitalWrite(LED_BUILTIN,LOW);

  // Shift/re-init MCP state:
  if(mcpstate==0)
  {
    mcpstate=1;
  }
  else
  {
    mcpstate = mcpstate << 1;
  }

  Serial.println(mcpstate);
  // set MCP state:
  mcp.writeGPIOAB(mcpstate);
  
  delay(PERIOD/2);
  digitalWrite(LED_BUILTIN,HIGH);

}
