classdef microfluidics < handle & hgsetget

    
    properties
        ComPort = 'COM7'
    end
    properties (SetAccess = private, SetObservable)
        Serial;
        state = 0;
        interactiveON = false;
        valves;
        pumps;
    end
    properties (Constant)
        numchannels = 8;
    end
    properties (Constant, GetAccess = private)
        Serial_timeout = 3 % In seconds
        Serial_BaudRate = 9600;
    end
    
    methods

            
            function connect(obj)
                obj.Serial = serial(obj.ComPort);
                obj.Serial.BaudRate=obj.Serial_BaudRate;
                set(obj.Serial,'Timeout',obj.Serial_timeout);
                obj.Serial.BytesAvailableFcnMode = 'terminator';
%                 obj.Serial.BytesAvailableFcn = @obj.callback;
                fopen( obj.Serial );
                
                readline = fgetl(obj.Serial);
                while(~strfind(readline, 'Setup done!'))
                    readline = fgetl(obj.Serial);
                end
                
                % setup valves and pumps
                for ind1 = 1:obj.numchannels
                    valves(ind1) = valve(ind1);
                    obj.sendcommand(valves(ind1).turnoff()); % Just to make sure
                    pumps(ind1) = pump(ind1);
                    obj.sendcommand(pumps(ind1).turnoff());
                end
                obj.valves = valves;
                obj.pumps = pumps;
           
            end
            
            %%%%%%%%%%%% Valves
            function valveon(obj, number)
                for indnumber = number
                    obj.sendcommand(obj.valves(indnumber).turnon());
                end
            end
            
            function valveoff(obj, number)
                for indnumber = number
                    obj.sendcommand(obj.valves(indnumber).turnoff());
                end
            end
            
            function setvalvestate(obj, number, state)
                for indnumber = number
                    obj.sendcommand(obj.valves(indnumber).setstate(state));
                end
                        
            end
            
            function setpwm(obj, number, varargin)
                for indnumber = number
                    obj.sendcommand(obj.valves(indnumber).pwm(varargin{:}));
                end
            end
            
            %%%%%%%%%%%% Pumps
            function pumpon(obj,number)
                for indnumber = number
                    obj.sendcommand(obj.pumps(indnumber).turnon());
                end
            end
            
            function pumpoff(obj,number)
                for indnumber = number
                    obj.sendcommand(obj.pumps(indnumber).turnoff());
                end
            end
            
            function setpumpstate(obj,number, state)
                for indnumber = number
                    if state
                        obj.pumpon(indnumber);
                    else
                        obj.pumpoff(indnumber);
                    end
                end
            end
            
            function setflowrate(obj,number, flowrate)
                for indnumber = number
                    obj.sendcommand(obj.pumps(indnumber).setflowrate(flowrate));
                end
            end
                

    end
    methods (Access = private)
        function sendcommand(obj, command)
            fprintf(obj.Serial,command);
            pause(.01) % give it time to breathe
        end
    end
end


            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            