classdef valve < handle & hgsetget
    
    properties
        channel;
        ison = false;
        pwmon = 0;
        pwmperc = 0;
        period = 1000;
    end
    
    methods
        function obj = valve(channel)
            obj.channel = channel;
        end
        
        function cmd = turnon(obj)
            obj.ison = true;
            cmd = obj.applystate();
        end
        
        function cmd = turnoff(obj)
            obj.ison = false;
            cmd = obj.applystate();
        end
        
        function cmd = toggle(obj)
            obj.ison = ~obj.ison;
            cmd = obj.applystate();
        end
        
        function cmd = setstate(obj, state)
            obj.ison = state;
            cmd = obj.applystate();
        end
        
        function cmd = pwm(obj,varargin)
            ip = inputParser();
            ip.addParameter('active',obj.pwmon);
            ip.addParameter('percentage',obj.pwmperc);
            ip.addParameter('period',obj.period);
            ip.parse(varargin{:});
            
            obj.pwmon = ip.Results.active;
            obj.pwmperc = ip.Results.percentage;
            obj.period = ip.Results.period;
            
            cmd = obj.applypwm();
        end
        
    end
    
    methods (Access = private)
        
        function cmd = applypwm(obj)
            cmd = sprintf('PWM%03dS%1d%%%03dP%d;', ...
                obj.channel,obj.pwmon,obj.pwmperc,obj.period);
        end
        
        function cmd = applystate(obj)
            cmd = sprintf('VAL%03dS%1d;',obj.channel, obj.ison);
        end
    end
end