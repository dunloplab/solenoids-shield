% Connect to the arduino:
mf = microfluidics();
mf.ComPort = 'COM9';
mf.connect();




%% set pumps flow rate:
flowrate = 36; % (ml/hr)
for ind1 = 1:8
    mf.setflowrate(ind1,flowrate);
    pause(.2);
end

%% Turn pumps on:
for ind1 = 1:8
    mf.pumpon(ind1);
    pause(.2);
end




%%
disp([datestr(now) ' Starting experiment (media 1)...']);
pause(3600*8) % wait for 8 hours


disp([datestr(now) ' Switching to media 2...'])
% Turn valves on: (media2)
for ind1 = [1:3 5:8] %for now valve 4 not online
    mf.valveon(ind1);
    pause(.2);
end

disp([datestr(now) ' Increasing flow rate for 1 minute...'])
% Turn pumps off:
for ind1 = 1:8
    mf.pumpoff(ind1);
    pause(.2);
end

% increase flow rate:
flowrate = 18; % (ml/hr)
for ind1 = 1:8
    mf.setflowrate(ind1,flowrate);
    pause(.2);
end

% Turn pumps on:
for ind1 = 1:8
    mf.pumpon(ind1);
    pause(.11);
end

pause(60) % Wait for 1 minute to flow new media through the chip.

disp([datestr(now) ' Going back to normal flow rate...'])
% Turn pumps off:
for ind1 = 1:8
    mf.pumpoff(ind1);
    pause(.2);
end

% increase flow rate:
flowrate = 1.8; % (ml/hr)
for ind1 = 1:8
    mf.setflowrate(ind1,flowrate);
    pause(.2);
end

% Turn pumps on:
for ind1 = 1:8
    mf.pumpon(ind1);
    pause(.11);
end






pause(3600*8) % wait for 8 hours


disp([datestr(now) ' Switching back to media 1...'])
% Turn valves off: (media1)
for ind1 = [1:4 5:8] %for now valve 4 not online
    mf.valveoff(ind1);
    pause(.2);
end

disp([datestr(now) ' Increasing flow rate for 1 minute...'])
% Turn pumps off:
for ind1 = 1:8
    mf.pumpoff(ind1);
    pause(.2);
end

% increase flow rate:
flowrate = 18; % (ml/hr)
for ind1 = 1:8
    mf.setflowrate(ind1,flowrate);
    pause(.2);
end

% Turn pumps on:
for ind1 = 1:8
    mf.pumpon(ind1);
    pause(.11);
end

pause(60) % Wait for 1 minute to flow new media through the chip.

disp([datestr(now) ' Going back to normal flow rate...'])
% Turn pumps off:
for ind1 = 1:8
    mf.pumpoff(ind1);
    pause(.2);
end

% increase flow rate:
flowrate = 1.8; % (ml/hr)
for ind1 = 1:8
    mf.setflowrate(ind1,flowrate);
    pause(.2);
end

% Turn pumps on:
for ind1 = 1:8
    mf.pumpon(ind1);
    pause(.11);
end
