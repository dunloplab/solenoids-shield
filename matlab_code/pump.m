classdef pump < handle & hgsetget
    
    properties
        channel;
        active = false;
        flowrate = 180; %ml/hour
    end
    properties (SetAccess = private, SetObservable)
        period; % in ms
    end
    properties (Constant)
        ul_per_stroke = 10; % volume (µl) moved per pump stroke
    end
    
    methods
        
        function obj = pump(channel)
            obj.channel = channel;
            obj.setperiod();
        end
        
        function cmd = turnon(obj)
            obj.active = true;
            cmd = obj.applystate();
        end
        
        function cmd = turnoff(obj)
            obj.active = false;
            cmd = obj.applystate();
        end
        
        function cmd = toggle(obj)
            obj.active = ~obj.active;
            cmd = obj.applystate();
        end
        
        function cmd = setflowrate(obj, flowrate)
            obj.flowrate = flowrate;
            obj.setperiod();
            cmd = obj.applystate();
        end
    end
    
    methods (Access = private)
        
        function cmd = applystate(obj)
            cmd = sprintf('PUM%03dS%1dP%d;', ...
                obj.channel,obj.active,obj.period);
        end
        
        function setperiod(obj)
            obj.period = (obj.ul_per_stroke/obj.flowrate)*3600;
        end
    end
end