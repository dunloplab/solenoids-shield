% Connect to the arduino:
mf = microfluidics();
mf.ComPort = 'COM7';
mf.connect();




%% set pumps flow rate:
flowrate = 30; % (ml/hr) 1.8/30
for ind1 = 1:8
    mf.setflowrate(ind1,flowrate);
    pause(.1);
end

%% Turn pumps on:
for ind1 = 1:8
    mf.pumpon(ind1);
    pause(.2);
end

%% Turn pumps off:
for ind1 = 1:8
    mf.pumpoff(ind1);
    pause(.2);
end




%% Turn valves on: (media2)
for ind1 = [1:8] %for now valve 4 not online
    mf.valveon(ind1);
    pause(1);
end

%% Turn valves off: (media1)
for ind1 = [1:8] %for now valve 4 not online
    mf.valveoff(ind1);
    pause(1);
end
